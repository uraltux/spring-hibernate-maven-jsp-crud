package ru.kostyan.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.kostyan.DAO.UserDAO;
import ru.kostyan.model.User;

import javax.transaction.Transactional;
import java.util.List;
@Service
public class UserServiceImpl implements UserService {
    private UserDAO userDAO;
    @Autowired
    public void setUserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
    @Transactional
    public void add(User user) {
        userDAO.add(user);
    }
    @Transactional
    public void delete(User user) {
        userDAO.delete(user);
    }
    @Transactional
    public void edit(User user) {
        userDAO.edit(user);
    }
    @Transactional
    public User getById(Long id) {
        return userDAO.getById(id);
    }
    @Transactional
    public List<User> getAllUsers() {
        return userDAO.getAllUsers();
    }
}
