package ru.kostyan.service;

import ru.kostyan.model.User;

import java.util.List;

public interface UserService {
    void add(User user);
    void delete(User user);
    void edit(User user);
    User getById(Long id);
    List<User> getAllUsers();
}
