<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: ant1kvar
  Date: 15.11.2019
  Time: 19:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>UserManager</title>
    <h1>user crud app</h1>
</head>
<body>
<table>
    <tr>
        <th>Id</th>
        <th>name</th>
        <th>email</th>
        <th>isItProgrammer</th>
        <th>action</th>
    </tr>
    <c:forEach var="user" items="${usersList}">
        <tr>
        <td>${user.id}</td>
        <td>${user.name}</td>
        <td>${user.email}</td>
        <td>${user.programmer}</td>
        <td>
            <a href="/edit/${user.id}">edit</a>
            <a href="/delete/${user.id}">delete</a>
        </td>
        </tr>
    </c:forEach>

</table>
<h2>Add</h2>
<c:url value="/add" var="add"/>
<a href="${add}">Add new user</a>
</body>
</html>
