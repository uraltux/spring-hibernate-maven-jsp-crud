<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Edit users data</title>
           <h1>Edit User</h1>
</head>
<body>

<form action="/edit" method="POST">
    <c:if test="${!empty user.id}">
        <input type="hidden" name="id" value="${user.id}">
    </c:if>
    <label for="name">name</label>
    <input type="text" name="name" id="name" value="${user.name}">
    <label for="name">email</label>
    <input type="text" name="email" id="email" value="${user.email}">
    <label for="Programmer">Programmer</label>
    <input type="checkbox" name="Programmer" id="Programmer" value="${user.programmer}">
    <input type="submit" value="Edit user">
</form>
</body>
</html>